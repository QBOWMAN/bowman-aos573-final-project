Drier-get-hotter and the ENSO cycle, repository information and abstract

According to Byrne 2021, modeled changes in temperature are well correlated with specific humidity.  While working on this project using the HadISD weather station dataset, I was not able to find such a relationship in the hisotrical temperature and humidity records.  Upon further research, it is likely that the local temperature and climate variability is larger than the signal resulting from the proposed "drier-get-hotter" mechanism, which may explain why I was not able to find the relationship.  However, global troposphere warming as a result of the ENSO cycle may be a more prominent signal over a wider area and more stations that is not necessarily a function of climate change, so this "drier-get"hotter" mechanism may be more visible in a ENSO-specific humidity-temperature correlation. To test this, I took weather station from the hadISD dataset, grouped their temperature and specific humidity data by ENSO cycles as a proxy for tropospheric warming, and then correlated the temperature and specific humidity relationship for each day.  In this initial analysis, it is shown that the average El Nino day is drier and hotter than the average La Nina day, corroborating the results from Byrne, 2021.

Used class packages: (AOS 573 environment is fine here)
pandas
numpy
matplotlib
xarray

Data used:
nino 3.4 anomaly index (see: https://psl.noaa.gov/gcos_wgsp/Timeseries/Nino34/ )
hadISD WMO weather station dataset, from revision 3.1.2.202106 (only 4 stations used here) (see: https://www.metoffice.gov.uk/hadobs/hadisd/ ) 

Only WMO weather stations between 20 S and 20N (tropics) are valid in this research, and due to time and storage limitations, only 4 stations are used in this sample.

The nino3.4 dataset has also been slightly modified for easier use with pandas: I have added column titles for easier importing and sorting.  It turns out this was unnecessary as I later decided to use it as a single 1-dimensional timeseries, but I didn't want to mess with it further so the header stays.

Repository contents:
/samplehum2: a directory with 4 sample weather station datasets in .nc format from the HadIISD dataset, somewhat randomly chosed
/image : directory containing image '50y2y.png' of (poorly presented) results from my last attempt at analyzing this data.
nino34.long.anom.data : El Nino index anomaly dataset (from https://psl.noaa.gov/gcos_wgsp/Timeseries/Nino34/ )
'Drier-get-hotter and ENSO.ipynb' : jupyter notebook file containing data analysis and presentation from this project
README.txt: this file, containing general information on the repository

dgh.yml: python environment for running this jupyter notebook (copy of AOS573 environment)


References:

    HadISD dataset related:
Dunn, R. J. H., (2019), HadISD version 3: monthly updates, Hadley Centre Technical Note

Dunn, R. J. H., et al. (2016), Expanding HadISD: quality-controlled, sub-daily station data from 1931, Geoscientific Instrumentation, Methods and Data Systems, 5, 473-491

Dunn, R. J. H., et al. (2014), Pairwise homogeneity assessment of HadISD, Climate of the Past, 10, 1501-1522

Dunn, R. J. H., et al. (2012), HadISD: A Quality Controlled global synoptic report database for selected variables at long-term stations from 1973-2011, Climate of the Past, 8, 1649-1679

Smith, A., et al. (2011): The Integrated Surface Database: Recent Developments and Partnerships. Bulletin of the American Meteorological Society, 92, 704-708


    nino3.4 dataset related:
Rayner N. A., D. E. Parker, E. B. Horton, C. K. Folland, L. V. Alexander, D. P. Rowell, E. C. Kent, A. Kaplan, Global analyses of sea surface temperature, sea ice, and night marine air temperature since the late nineteenth century, J. Geophys. Res., 108 (D14), 4407, doi:10.1029/2002JD002670, 2003.


    Theory related:
Byrne, Michael P. "Amplified warming of extreme temperatures over tropical land." Nature Geoscience 14.11 (2021): 837-841.

Chiang, John CH, and Benjamin R. Lintner. "Mechanisms of remote tropical surface warming during El Niño." Journal of climate 18.20 (2005): 4130-4149.